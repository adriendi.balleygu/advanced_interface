# Good Guy Greg

## Description du travail

Le but est de faire gagner du temps au personnel soignant et d'avertir les patients dans les situations où une infirmière ne serait pas immédiatement disponible pour une requête simple. Le but peut aussi être de soulager le personnel soignant dans les cas où un patient est connu pour trop souvent solliciter leur attention

Pour ce travail, la technologie étudiée est DialogFlow, qui permet de créer simplement un assistant vocal.

## Comment installer la démo

* Télécharger le fichier GoodGuyGreg.zip (ne pas le décompresser)

* Se rendre sur DialogFlow, créer un nouvel agent et cliquer sur la molette en haut à gauche à côté du nom de l'agent

* Se rendre dans l'onglet Export and Import, cliquer sur le bouton Import from zip et choisir le fichier GoodGuyGreg.zip

* Ecrire IMPORT dans la barre de saisie et cliquer sur le bouton Import, puis Done.


On peut directement essayer l'assistant sur DialogFlow, ou se rendre dans l'onglet Integrations, et choisir sur quelle plateforme on désire l'intégrer.

L'application supporte des phrases du type "J'aimerais que l'on m'apporte quelque chose", "J'ai mal à la tête", "J'aimerais voir telle ou telle infirmière". Le prénom de l'infirmière ainsi que l'objet de la demande du patient sont reconnus et extraits.
